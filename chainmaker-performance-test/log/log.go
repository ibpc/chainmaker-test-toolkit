/*
Copyright (C) 2023 Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package logger

import (
	"log"
	"os"
	"time"
)

var Logger *log.Logger

func BeginLog() {

	// 获取当前时间
	currentTime := time.Now()

	// 格式化时间为精确到小时的字符串，格式为：年月日时（如：2023072517）
	timeString := currentTime.Format("2006010215")
	filename := "chainmaker_performance_test.log." + timeString

	// 打开文件用于写入日志
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Printf("无法打开日志文件：%s", err)
	}

	// 创建一个新的Logger实例，将输出目标设置为文件
	Logger = log.New(file, "", log.LstdFlags|log.Lshortfile)
}
