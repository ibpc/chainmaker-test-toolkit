/*
Copyright (C) 2023 Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package parallel

import (
	"chain-performance-test/chainclient"
	"chain-performance-test/datahandler"
	logger "chain-performance-test/log"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"fmt"
	"github.com/go-yaml/yaml"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	UserNameOrg1Admin1    = "org1admin1" // admin节点名
	UserNameOrg2Admin1    = "org2admin1"
	UserNameOrg3Admin1    = "org3admin1"
	UserNameOrg4Admin1    = "org4admin1"
	createContractTimeout = int64(50)
	claimVersion          = "2.0.0"
)

var (
	Client                *sdk.ChainClient       // 被压测的链
	Clients1              []*sdk.ChainClient     // 被压测的链数组
	sdkConfigPaths        []string               // 被压测的cert模式链配置文件数组
	sdkPKConfigPaths      []string               // 被压测的public模式链配置文件数组
	SdkPWKConfigPaths     []string               // 被压测的permissionedWithKey模式链配置文件数组
	Clientslen            int                    // 被压测的链个数
	ClaimByteCodePath     string                 // 存证类合约智能合约路径
	txCounts              uint32             = 0 // 成功上链交易数
	timeStart             time.Time              // 压测开始时间
	timeEnd               time.Time              // 压测开始时间
	BlockStart            int64                  // 压测开始区块
	Wg                    = sync.WaitGroup{}
	ConfigurableParameter *ConfigurableParameters // 可配置参数
	ClientsParameter      *ClientsParameters      // 多加点加压个数参数
	Model                 string                  // 身份认证模型
	ContractName          string                  // 智能合约名称
	ContractType          string                  // 智能合约类型
	RuntimeTypeString     string                  // 智能合约语言类型, string类型
	RuntimeType           common.RuntimeType      // 智能合约语言类型, common.RuntimeType类型
	contractMethod        string                  // 智能合约被压测方法
	Params                string                  // 智能合约被压测方法参数
	ThreadNum             int                     // 单次并发进程数
	LoopNum               int                     // 压测并发次数
	SleepTime             int                     // 并发间隔,单位ms
	ClimbTime             int                     // 爬坡时间,单位s
)

// ConfigurableParameters 可配置参数定义
type ConfigurableParameters struct {
	ChainParameters    *ChainParameters    `yaml:"chain_information"`                // 长安链可配置参数
	ContractParameters *ContractParameters `yaml:"contract_configurable_parameters"` // 智能合约可配置参数
	PressureParameters *PressureParameters `yaml:"pressure_configurable_parameters"` // 并发压测可配置参数
}

// ChainParameters 智能合约可配置参数定义
type ChainParameters struct {
	Model string `json:"model"` //身份权限管理
}

// ContractParameters 智能合约可配置参数定义
type ContractParameters struct {
	ContractName   string `yaml:"contract_name"`   // 智能合约名称参数
	ContractType   string `yaml:"contract_type"`   // 智能合约类型参数
	RuntimeType    string `yaml:"runtime_type"`    // 智能合约语言类型参数
	ContractMethod string `yaml:"contract_method"` // 智能合约被压测方法参数
	Params         string `yaml:"params"`          // 智能合约被压测方法参数列表参数
}

// PressureParameters 并发压测可配置参数定义
type PressureParameters struct {
	ThreadNum int `yaml:"thread_num"` // 单次并发进程数参数
	LoopNum   int `yaml:"loop_num"`   // 压测并发次数参数
	SleepTime int `yaml:"sleep_time"` // 并发间隔参数,单位ms
	ClimbTime int `yaml:"climb_time"` // 并发间隔参数,单位ms
}

type ClientsParameters struct {
	NodeNum int `yaml:"node_num"` // 长安链多节点加压，加压节点个数
}

type ContractCreator struct{}

// InitBeforeTest 并发压测初始化
func InitBeforeTest() {

	//1.解析可配置参数
	InitConfig()
	//2.安装claim智能合约
	var clientCreator chainclient.ClientCreate
	var contractClaimCreator ContractCreator
	if "Claim" == ContractType {
		err := ClaimContractInstance(clientCreator, contractClaimCreator)
		if err != nil {
			logger.Logger.Panic("安装智能合约失败:", err)
		}
	}
	if "Query" == ContractType {
		err := ClaimContractInstance(clientCreator, contractClaimCreator)
		if err != nil {
			logger.Logger.Panic("安装智能合约失败:", err)
		}
	}
	//3.安装asset智能合约+注册转账用户
}

// InitConfig 初始化可配置参数
func InitConfig() {

	yamlFile1, err := os.ReadFile("./config/clients.yml")
	if err != nil {
		logger.Logger.Panic("打开文件失败:", err)
	}
	err = yaml.Unmarshal(yamlFile1, &ClientsParameter)
	if err != nil {
		logger.Logger.Panic(err)
	}
	Clientslen = ClientsParameter.NodeNum // 节点个数

	yamlFile, err := os.ReadFile("./config/const_config.yml")
	if err != nil {
		logger.Logger.Panic("打开文件失败:", err)
	}
	err = yaml.Unmarshal(yamlFile, &ConfigurableParameter)
	if err != nil {
		logger.Logger.Panic(err)
	}
	num := fmt.Sprintf("%08v", rand.Intn(99999999))
	Model = ConfigurableParameter.ChainParameters.Model                         // 身份认证模式
	ContractName = ConfigurableParameter.ContractParameters.ContractName + num  // 智能合约名称
	ContractType = ConfigurableParameter.ContractParameters.ContractType        // 智能合约类型
	RuntimeTypeString = ConfigurableParameter.ContractParameters.RuntimeType    // 智能合约语言类型
	contractMethod = ConfigurableParameter.ContractParameters.ContractMethod    // 智能合约被压测方法
	ThreadNum = ConfigurableParameter.PressureParameters.ThreadNum / Clientslen // 单次并发进程数
	LoopNum = ConfigurableParameter.PressureParameters.LoopNum                  // 压测并发次数
	SleepTime = ConfigurableParameter.PressureParameters.SleepTime              // 并发间隔,单位ms
	ClimbTime = ConfigurableParameter.PressureParameters.ClimbTime              // 爬坡时间,单位s
	Params = ConfigurableParameter.ContractParameters.Params                    // 智能合约被压测方法参数

	// runtimeType 参数类型修改
	RuntimeTypeModification()

	// 链配置信息生成
	if Model == "PermissionWithCert" {
		SdkConfigPathsMake()
	} else if Model == "Public" {
		SdkPKConfigPathsMake()
	} else if Model == "PermissionWithKey" {
		SdkPWKConfigPathsMake()
	}

	// byteCodePathMake 智能合约路径修改
	ByteCodePathMake()

	// 参数列表解析
	datahandler.ParametersList = datahandler.Parameters{} //初始化
	datahandler.ParametersList.ContractParametersList.ContractFunctionParametersMap = make(map[string]string)
	FunctionParametersList := strings.Split(Params, datahandler.ParaListSep)
	for i := 0; i < len(FunctionParametersList); i++ {
		FunctionParameters := strings.Split(FunctionParametersList[i], datahandler.ParaSep)
		datahandler.ParametersList.ContractParametersList.ContractFunctionParametersMap[FunctionParameters[0]] = FunctionParameters[1]
	}

	logger.Logger.Println("====================== 可配置参数 ======================")
	logger.Logger.Printf("config.ContractParameters: %#v\n", ConfigurableParameter.ContractParameters)
	logger.Logger.Printf("config.PressureParameters: %#v\n", ConfigurableParameter.PressureParameters)
}

// ClaimContractInstance 安装Claim智能合约
func ClaimContractInstance(clientCreator chainclient.ClientCreator, contractClaimCreator chainclient.ContractClaimCreator) error {
	var err error
	Clients1 = make([]*sdk.ChainClient, Clientslen)
	if Model == "PermissionWithCert" {
		for i := 0; i < Clientslen; i++ {
			Clients1[i], err = clientCreator.CreateClientWithConfig(sdkConfigPaths[i])
		}
	} else if Model == "Public" {
		for i := 0; i < Clientslen; i++ {
			Clients1[i], err = clientCreator.CreateClientWithConfig(sdkPKConfigPaths[i])
		}
	} else if Model == "PermissionWithKey" {
		for i := 0; i < Clientslen; i++ {
			Clients1[i], err = clientCreator.CreateClientWithConfig(SdkPWKConfigPaths[i])
		}
	}
	if err != nil {
		return err
	}

	logger.Logger.Println("====================== 安装存证合约 ======================")
	// Admin name
	var usernames []string
	if Model == "PermissionWithCert" {
		usernames = []string{UserNameOrg1Admin1, UserNameOrg2Admin1, UserNameOrg3Admin1, UserNameOrg4Admin1}
	} else if Model == "Public" {
		usernames = []string{UserNameOrg1Admin1}
	} else if Model == "PermissionWithKey" {
		usernames = []string{UserNameOrg1Admin1, UserNameOrg2Admin1, UserNameOrg3Admin1, UserNameOrg4Admin1}
	}
	err = contractClaimCreator.UserContractClaimCreate(Clients1[0], true, usernames...)
	if err != nil {
		return err
	}
	return nil
}

// RuntimeTypeModification runtimeType参数类型修改
func RuntimeTypeModification() {
	switch RuntimeTypeString {
	case "INVALID":
		RuntimeType = common.RuntimeType_INVALID
	case "NATIVE":
		RuntimeType = common.RuntimeType_NATIVE
	case "WASMER":
		RuntimeType = common.RuntimeType_WASMER
	case "WXVM":
		RuntimeType = common.RuntimeType_WXVM
	case "GASM":
		RuntimeType = common.RuntimeType_GASM
	case "EVM":
		RuntimeType = common.RuntimeType_EVM
	case "DOCKER_GO":
		RuntimeType = common.RuntimeType_DOCKER_GO
	case "JAVA":
		RuntimeType = common.RuntimeType_JAVA
	case "GO":
		RuntimeType = common.RuntimeType_GO
	}
}

// SdkConfigPathsMake 生成cert模式链配置文件数组
func SdkConfigPathsMake() {
	sdkConfigPaths = make([]string, Clientslen)
	for i := 0; i < Clientslen; i++ {
		sdkConfigPaths[i] = "./config/sdk_config" + strconv.Itoa(i+1) + ".yml"
	}
}

// SdkPKConfigPathsMake 生成pubilc模式链配置文件数组
func SdkPKConfigPathsMake() {
	sdkPKConfigPaths = make([]string, Clientslen)
	for i := 0; i < Clientslen; i++ {
		sdkPKConfigPaths[i] = "./config/sdk_config_pk" + strconv.Itoa(i+1) + ".yml"
	}
}

// SdkPWKConfigPathsMake 生成permissionedWithKey模式链配置文件数组
func SdkPWKConfigPathsMake() {
	SdkPWKConfigPaths = make([]string, Clientslen)
	for i := 0; i < Clientslen; i++ {
		SdkPWKConfigPaths[i] = "./config/sdk_config_pwk" + strconv.Itoa(i+1) + ".yml"
	}
}

// ByteCodePathMake 示例智能合约路径修改
func ByteCodePathMake() {
	if RuntimeType == common.RuntimeType_WASMER {
		ClaimByteCodePath = "./contract/claim_demo/rustFact.wasm"
	}
	if RuntimeType == common.RuntimeType_DOCKER_GO {
		ClaimByteCodePath = "./contract/claim_demo/dockerFact230.7z"
	}
}

// UserContractClaimCreate 创建用户存证智能合约
func (con ContractCreator) UserContractClaimCreate(client *sdk.ChainClient, withSyncResult bool, usernames ...string) error {

	var kvs []*common.KeyValuePair
	// 安装用户智能合约
	resp, err := CreateUserContract(client, claimVersion, ClaimByteCodePath, kvs, withSyncResult, usernames...)
	// 记录压测前初始块高，用于消息订阅
	BlockStart = int64(resp.TxBlockHeight) + 1
	logger.Logger.Printf("BlockStart:%d\n", BlockStart)
	logger.Logger.Println("resp.ContractResult.Code:", resp.ContractResult.Code)
	logger.Logger.Println("resp.ContractResult.Message:", resp.ContractResult.Message)
	if err != nil {
		return err
	}
	return nil
}

// CreateUserContract 安装用户智能合约
func CreateUserContract(client *sdk.ChainClient, version,
	byteCodePath string, kvs []*common.KeyValuePair, withSyncResult bool, usernames ...string) (*common.TxResponse, error) {
	payload, err := client.CreateContractCreatePayload(ContractName, version, byteCodePath, RuntimeType, kvs)
	if err != nil {
		logger.Logger.Panic(err)
	}
	// 各组织Admin权限用户签名
	endorsers, err := chainclient.GetEndorsersWithAuthType(client.GetHashType(), client.GetAuthType(), payload, usernames...)
	if err != nil {
		logger.Logger.Panic(err)
	}

	// 发送请求
	resp, err := client.SendContractManageRequest(payload, endorsers, createContractTimeout, withSyncResult)
	if err != nil {
		return resp, err
	}

	// 检查交易是否成功
	err = chainclient.CheckProposalRequestResp(resp, true)
	return resp, err
}

// RandParams 随机化参数
func RandParams() []*common.KeyValuePair {
	num := rand.Int()
	// 获取带压测方法的参数列表
	set := datahandler.ParametersList.ContractParametersList.ContractFunctionParametersMap
	params := []*common.KeyValuePair{}
	// 构建随机的实际参数
	for k, v := range set {
		param := new(common.KeyValuePair)
		param.Key = k
		param.Value = []byte(fmt.Sprintf(v, num))
		params = append(params, param)
	}
	return params
}
