#! /bin/bash
#file:mysql_docker_install.sh
#company:cvnavi.com
#author:Pengjunlin
echo "当前执行文件......$0"
exists_name=""
exists_tag=""
# 下载 mysql
for i in [ `docker images ` ]; do
	
	if [[ $i == "docker.io/mysql" ]]; then
		echo "$i"
		exists_name="1"
	fi
	if [[ $i == "5.7" ]]; then
		echo "$i"
		exists_tag="1"
	fi
done
if [[ $exists_name == "1" && $exists_tag == "1" ]]; then
	echo "本地已存在mysql:5.7镜像."
else
	docker pull mysql:5.7
fi
# 停止mysql容器
for i in [ `docker ps ` ]; do
	if [[ $i == "mysql5.7" ]]; then
		echo "停止mysql5.7容器 "
		docker stop mysql5.7
		break
	fi
done
# 删除mysql容器
for i in [ `docker ps -a` ]; do
	if [[ $i == "mysql5.7" ]]; then
		echo "删除mysql5.7容器 "
		docker rm mysql5.7
		break
	fi
done
 
if [[ -f "/usr/local/docker_mysql_create_table.sh" ]]; then
	# 创建mysql映射目录
	if [[ ! -d "/etc/mysql" ]]; then
		mkdir /etc/mysql
	fi
	if [[ ! -d "/etc/mysql/conf.d" ]]; then
		mkdir /etc/mysql/conf.d
	fi
	if [[ ! -d "/etc/mysql/logs" ]]; then
		mkdir /etc/mysql/logs
	fi
	if [[ ! -d "/etc/mysql/data" ]]; then
		mkdir /etc/mysql/data
	fi
        echo "创建容器映射目录：/etc/mysql"
	# 运行容器实例
        echo "运行容器实例，映射至3306端口"
	docker run -d -p 3306:3306 --name mysql5.7 -v /etc/mysql/conf.d:/etc/mysql/conf.d -v /etc/mysql/logs:/logs -v /etc/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root  mysql:5.7
        echo "运行容器实例成功"